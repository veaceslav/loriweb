package main

import "fmt"
import "database/sql"
import _ "github.com/lib/pq"

const (
    DB_USER     = "postgres"
    DB_PASSWORD = "postgres"
    DB_NAME     = "loriweb"
)

var db *sql.DB

func init() {
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
        DB_USER, DB_PASSWORD, DB_NAME)
	dbconn, err := sql.Open("postgres", dbinfo)
	if err != nil {
		fmt.Println(err)
	}
	db = dbconn
}

func save(domain string) {
	fmt.Println("saving", domain)
	
	var id int
	
	err := db.QueryRow("INSERT INTO domains (domain) VALUES ($1)", domain).Scan(&id)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
}

func update(domain string, metaGenerator string) {
	fmt.Println("updating", domain)
	
	var id int
	
	err := db.QueryRow("UPDATE domains SET meta_generator = ($1) WHERE domain = ($2)", metaGenerator, domain).Scan(&id)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
}

func findAllNotChecked() ([]string, error) {
	fmt.Println("findAllNotChecked")
	
	rows, err := db.Query("SELECT domain FROM domains WHERE meta_generator IS NULL")
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	
	var lines []string
	for rows.Next() {
		var domain string
		err = rows.Scan(&domain)
        if err != nil {
			fmt.Println(err.Error())
			return nil, err
		}
		lines = append(lines, domain)
	}
	
	return lines, nil
}

func count() (int, error) {
	fmt.Println("count")
	
	var count int
	
	err := db.QueryRow("SELECT COUNT(domain_id) FROM domains").Scan(&count)
	if err != nil {
		fmt.Println(err.Error())
		return -1, err
	} 
	
	return count, nil
}