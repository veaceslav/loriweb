package main

import "fmt"
import "net/http"
import "io/ioutil"
import "regexp"

var stop = false

func Start() {
	fmt.Println("Starting")
	toChecked, _ := findAllNotChecked()
	fmt.Println("domains:", toChecked)
	stop = false
	
	for i := range toChecked {
		if !stop {
			doWork(toChecked[i])
		}
	}
}

func doWork(domain string) {
	client := &http.Client{}
	url := "http://" + domain + "/";
	request, err := http.NewRequest("GET", url, nil)
	request.Close = true
	
	response, err := client.Do(request)
	if err != nil {
		fmt.Println(domain, err)
		return
	}
	defer response.Body.Close()
	
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(domain, err)
		return
	}
	
	// regexp
	r, err := regexp.Compile("<meta name=\"generator\" content=\"(.*?)\" />")
	if err != nil {
		fmt.Println(domain, err)
		return
	}
	
	var metaGenerator string
	match := r.FindStringSubmatch(string(contents))
	if len(match) == 2 {
		metaGenerator = match[1]
	} else {
		metaGenerator = "not_found"
	}
	fmt.Println(domain, ":", metaGenerator)
	
	// update
	update(domain, metaGenerator)
}

func Stop() {
	fmt.Println("Stoping")
	stop = true
}