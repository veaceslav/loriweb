package main

import "fmt"
import "html/template"
import "io"
import "io/ioutil"
import "net/http"
import "os"

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("indexHandler")
	t, err := template.ParseFiles("templates/index.html")
    if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	
	t.ExecuteTemplate(w, "index", nil)
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("uploadHandler")
    t, err := template.ParseFiles("templates/upload.html")
    if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	
	t.ExecuteTemplate(w, "upload", nil)
}

func processUploadHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("processUploadHandler")
    file, header, err := r.FormFile("file")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	out, err := ioutil.TempFile(os.TempDir(), "loriweb_")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer os.Remove(out.Name())

	_, err = io.Copy(out, file)
	if err != nil {
		fmt.Println(err)
	}
	ProcesUpload(out.Name());

	fmt.Fprintf(w, "File uploaded successfully:", header.Filename)
}

func downloadHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("downloadHandler")
    t, err := template.ParseFiles("templates/download.html")
    if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	
	t.ExecuteTemplate(w, "download", nil)
}

func downloadStartHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("downloadStartHandler")
    go Start();
}

func downloadStopHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("downloadStopHandler")
    go Stop();
}

func statusHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("statusHandler")
    t, err := template.ParseFiles("templates/status.html")
    if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	
	checked, err := Checked()
	if err != nil {
		fmt.Fprintf(w, err.Error())
	}
	fmt.Println("checked", checked)
	
	t.ExecuteTemplate(w, "status", checked)
}

func main() {
	fmt.Println("Listening on port :3000")
	
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/upload", uploadHandler)
	http.HandleFunc("/upload/process", processUploadHandler)
	http.HandleFunc("/download", downloadHandler)
	http.HandleFunc("/download/start", downloadStartHandler)
	http.HandleFunc("/download/stop", downloadStopHandler)
	http.HandleFunc("/status", statusHandler)
    http.ListenAndServe(":3000", nil)
}