DROP DATABASE IF EXISTS loriweb;
CREATE DATABASE	loriweb

\c loriweb;

--
-- TABLE: hosts
-- 
CREATE TABLE hosts (
    host_id SERIAL NOT NULL PRIMARY KEY,
    host VARCHAR(255) UNIQUE NOT NULL,
    domain VARCHAR(255) NOT NULL,
    tld VARCHAR(255) NOT NULL,
    active BOOLEAN DEFAULT true
);

CREATE INDEX ON hosts (host_id)
CREATE INDEX ON hosts (host)