package main

import "fmt"

func Checked() (checked int, err error){
	count, err := count()
	if err != nil {
		fmt.Println(err)
		return -1, err
	}

	return count, nil
}