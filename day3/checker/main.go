package main

import (
    "fmt"
    "log"
    "net/http"
    // "runtime"
    "time"
	"os"
	"bufio"
	"strings"
	"net"
	"github.com/miekg/dns"
	"errors"
	"sync"
)

type Response struct {
    *http.Response
    err error
	hostname string
}

var dnslist []string
var dnsindex int = 0
var l sync.Mutex


// Dispatcher
func dispatcher(reqChan chan *http.Request) {
	defer close(reqChan)
	
	file, err := os.Open("./hosts.list")
    if err != nil {
        panic(err)
    }
    defer file.Close()
	
	scanner := bufio.NewScanner(file)
    for scanner.Scan() {
		hostname := scanner.Text()
		req, err := http.NewRequest("HEAD", "http://" + hostname + "/", nil)
        if err != nil {
            log.Println("ERROR", hostname, err)
        }
        reqChan <- req
	}
}

// Worker Pool
func workerPool(reqChan chan *http.Request, respChan chan Response) {
    client := &http.Client{
		Timeout: 5 * time.Second,
		Transport: &http.Transport{
			Dial: Dial,
		},
	}
    for i := 0; i < 1000; i++ {
        go worker(client, reqChan, respChan)
    }
}

// Worker
func worker(c *http.Client, reqChan chan *http.Request, respChan chan Response) {
    for req := range reqChan {
        resp, err := c.Do(req)
        r := Response{resp, err, req.URL.Host}
        respChan <- r
    }
}

// Consumer
func consumer(respChan chan Response) int {
	conns := 0;
	for r := range respChan {
        if r.err != nil {
            log.Println("ERROR", r.hostname, r.err)
        } else {
			log.Println("SUCCESS", r.StatusCode, r.Header.Get("Server"), r.Header.Get("X-Powered-By"), r.Header.Get("Location"))
        }
		conns++
    }
	return conns
}

func Dial(network, addr string) (net.Conn, error) {
	s := strings.Split(addr, ":")
    addrString, addrPort := s[0], s[1]

	var ip net.IP
	addrs, err := resolve(addrString, nameServer())
	if err != nil {
		log.Println("ERROR Dial: resolve error:", err.Error())
		return nil, err
	}
	
	if len(addrs) > 0 {
		ip = addrs[0]
		// log.Printf("Dial: resolved: %s -> %s", addr, ip.String())
		addr = ip.String()
	}
	
	// Return port back
	addr += ":" + addrPort
	conn, err := net.Dial(network, addr)
	if err != nil {
		return conn, err
	}
	
	tcpConn, ok := conn.(*net.TCPConn)
	if !ok {
		return conn, errors.New("ERROR Dial: conn->TCPConn type assertion failed.")
	}
	
	tcpConn.SetKeepAlive(false)
	tcpConn.SetLinger(0)
	tcpConn.SetNoDelay(true)
	
	return tcpConn, nil
}

func resolve(name, nameserver string) (addrs []net.IP, err error) {
	m := new(dns.Msg)
	m.SetQuestion(dns.Fqdn(name), dns.TypeA)
	m.MsgHdr.RecursionDesired = true

	c := &dns.Client{
		Net: "udp",
		ReadTimeout: 5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}
	
	r, _, err := c.Exchange(m, nameserver + ":53")
	if err != nil {
		log.Println("ERROR Got error result from exchange", nameserver)
		return nil, err
	}
	
	if r.Rcode != dns.RcodeSuccess {
		log.Println("ERROR Invalid result from exchange")
		return nil, errors.New("Invalid result from exchange.")
	}
	
	addrs = make([]net.IP, 0)
	for _, a := range r.Answer {
		if rra, ok := a.(*dns.A); ok {
			addrs = append(addrs, rra.A)
		}	
	}
	return addrs, nil
}

func nameServer() string {
	l.Lock()
	if dnsindex < len(dnslist) - 1 {
		dnsindex += 1
	} else {
		dnsindex = 0
	}	
	l.Unlock()
	return dnslist[dnsindex]
}

func main() {
	file, err := os.Open("./dns.list")
    if err != nil {
        panic(err)
    }
    defer file.Close()
	
	scanner := bufio.NewScanner(file)
    for scanner.Scan() {
		ns := scanner.Text()
		dnslist = append(dnslist, ns)
	}

    // runtime.GOMAXPROCS(runtime.NumCPU())
	
    reqChan := make(chan *http.Request)
    respChan := make(chan Response)
	
    go dispatcher(reqChan)
    go workerPool(reqChan, respChan)
	
    conns := consumer(respChan)
    fmt.Printf("Connections:\t%d", conns)
}