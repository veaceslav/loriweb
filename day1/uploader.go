package main

import "bufio"
import "fmt"
import "os"

func ProcesUpload(fileName string) {
	fmt.Println("ProcesUpload...", fileName)
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println(err)
		return
	}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	
	counter := 0
	for scanner.Scan() {
		counter++
		line := scanner.Text()
		fmt.Println(line)
		save(line)
	}
	fmt.Println("Counter", counter)
}