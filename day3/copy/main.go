package main

import "bufio"
import "fmt"
import "log"
import "os"
import "strings"

import "database/sql"
import "github.com/lib/pq"

type Host struct {
	HostId 		int
	Hostname 	string
	Domain 		string
	Tld 		string
	Active 		bool
}

// 1. Reading large file
// 2. Bulk insert into PostgreSQL
func main() {
	log.Println("Initialize")
	
	file, err := os.Open("./hosts.list")
    if err != nil {
        panic(err)
    }
    defer file.Close()
	
	db, err := sql.Open("postgres", "user=postgres password=postgres dbname=webzzz_demo sslmode=disable")
	if err != nil {
		panic(err)
	}

	counter := 0
	hosts := make([]Host, 0)
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        counter++
		hostname := scanner.Text()
		host := Host{
				Hostname: hostname, 
				Domain: fetchDomain(hostname), 
				Tld: fetchTld(hostname),
				}
		hosts = append(hosts, host)
		if counter % 50000 == 0 {
			log.Println(counter)
			bulkCopy(db, hosts)
			// bulkSave(db, hosts)
			hosts = hosts[:0]
		}
    }
	bulkCopy(db, hosts)
	// bulkSave(db, hosts)
	log.Println("Total hosts:", counter)
}

func fetchDomain(hostname string) string {
	// TEST
	return hostname
}

func fetchTld(hostname string) string {
	// TEST
	return hostname
}

func bulkCopy(db *sql.DB, hosts []Host) {
	tx, err := db.Begin()
	if err != nil {
		log.Println(err)
	}

	stmt, err := tx.Prepare(pq.CopyIn("hosts", "host", "domain", "tld"))
	if err != nil {
		log.Println(err)
	}
	
	for idx := range hosts {
		_, err = stmt.Exec(hosts[idx].Hostname, hosts[idx].Domain, hosts[idx].Tld)
		// log.Println(hosts[idx].Hostname, hosts[idx].Domain, hosts[idx].Tld)
		if err != nil {
			log.Println(err)
		}
	}
	
	_, err = stmt.Exec()
	if err != nil {
		log.Println(err)
	}
	
	err = stmt.Close()
	if err != nil {
		log.Println(err)
	}
	
	err = tx.Commit()
	if err != nil {
		log.Println(err)
	}
}

func bulkSave(db *sql.DB, hosts []Host) {
    values := make([]string, 0, len(hosts))
    params := make([]interface{}, 0, len(hosts) * 3)
    
	i := 0
	for _, host := range hosts {
		query := fmt.Sprint("($", i*3+1, ", $", i*3+2, ", $", i*3+3, ")")
        values = append(values, query)
        params = append(params, host.Hostname)
        params = append(params, host.Domain)
        params = append(params, host.Tld)
		// log.Println(host.Hostname, host.Domain, host.Tld)
		i++
    }
    
	stmt := fmt.Sprintf("INSERT INTO hosts (host, domain, tld) VALUES %s", strings.Join(values, ","))
	
	log.Println(stmt)
	log.Println(params...)
    
	_, err := db.Exec(stmt, params...)
    if err != nil {
		log.Println(err)
	}
}